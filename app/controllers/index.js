var args = arguments[0] || {};
var self = $;
var source = args['source'] || '-no args-';
var intent;
var activity = Ti.Android.currentActivity;

console.log('MJR index.js source ' + source + ', intent ' + Alloy.Globals.launchUrl);
console.log('MJR index.js currentActivity.intent is ' + Ti.Android.currentActivity.intent.data);

intent = Alloy.Globals.launchUrl || '[no intent]';
$.label.text = intent;

function doClick(e) {
    alert($.label.text);
}

var onNewIntent = function(e) {
	console.log("MJR index.js newintent " + e.intent.data);
/*
	if (e.intent.data != intent) {
		console.log("MJR closing window because of new intent (" + intent + " vs " + e.intent.data);
		activity.removeEventListener('newintent', onNewIntent);
		win.close();
	}
*/
	intent = e.intent.data;
	$.label.text = intent;
};

activity.addEventListener('newintent', onNewIntent);

var win = $.index;

var close = function() {
	Ti.API.info('MJR Window close event fired');
	activity.removeEventListener('newintent', onNewIntent);
	win.removeEventListener('close', close);
};

win.addEventListener('close', close);

/*
$.index.addEventListener('androidback', function(e) {
	console.log('MJR androidback');
	//win.close();
	//e.cancelBubble = true;
});
*/
/*
var receiveMagicLink = function(args) {
	console.log('MJR receiveMagicLink intent ' + args['intent'].data);
	self.label.text = args['intent'].data;
};
Ti.App.addEventListener('receiveMagicLink', receiveMagicLink);
*/

/*
activity.onCreate = function(e) {
	Ti.API.info('MJR onCreate ' + JSON.stringify(e));
	Alloy.Globals.mjr = true;
};
activity.onDestroy = function(e) {
	Ti.API.info('MJR onDestroy ' + JSON.stringify(e));
	activity.removeEventListener('newintent', onNewIntent);
};
activity.onPause = function(e) {
	Ti.API.info('MJR onPause ' + JSON.stringify(e));
};

activity.onRestart = function(e) {
	Ti.API.info('MJR onRestart ' + JSON.stringify(e));
};

activity.onResume = function(e) {
	Ti.API.info('MJR onResume ' + JSON.stringify(e));
};

activity.onStart = function(e) {
	Ti.API.info('MJR onStart ' + JSON.stringify(e));
};

activity.onStop = function(e) {
	Ti.API.info('MJR onStop ' + JSON.stringify(e));
};
*/
$.index.open(); 
