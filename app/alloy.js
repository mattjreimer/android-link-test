// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
var inited = false;
Alloy.Globals.mjr = false;
console.log('MJR in alloy.js');
if (OS_ANDROID) {
	/*
	 function resumeLauncher() {
	 	if (inited) {
	 		// recreate activity
	 		console.log("MJR resumeLauncher recreating controller");
	 		Alloy.createController('index', { source : 'resume' });
	 	} else {
	 		console.log("MJR resumeLauncher NOT creating controller");
	 	}
	 	inited = true;
	 }

	 if (Ti.Android.currentActivity) {
	 	console.log("MJR alloy.js currentActivity is set");
	 	Ti.Android.currentActivity.setOnResume(resumeLauncher);
	 }
	 */

	var onNewIntent = function(e) {
		console.log("MJR alloy.js newintent " + e.intent.data);

		if (e.intent.data != Alloy.Globals.launchUrl) {
			console.log("MJR alloy.js creating new controller");
			Alloy.Globals.launchUrl = e.intent.data;
			Alloy.createController('index', { source : 'newintent' });
		}

	};
	Ti.Android.currentActivity.addEventListener('newintent', onNewIntent);

	Alloy.Globals.launchUrl = Ti.Android.currentActivity.intent.data;

	/*
	var activity = Ti.Android.currentActivity;

	 activity.onCreate = function(e) {
	 Ti.API.info('MJR alloy onCreate ' + JSON.stringify(e));
	 };
	activity.onDestroy = function(e) {
		Ti.API.info('MJR alloy onDestroy ' + JSON.stringify(e));
	};
	 activity.onPause = function(e) {
	 Ti.API.info('MJR alloy onPause ' + JSON.stringify(e));
	 };

	 activity.onRestart = function(e) {
	 Ti.API.info('MJR alloy onRestart ' + JSON.stringify(e));
	 };

	 activity.onResume = function(e) {
	 Ti.API.info('MJR alloy onResume ' + JSON.stringify(e));
	 };

	 activity.onStart = function(e) {
	 Ti.API.info('MJR alloy onStart ' + JSON.stringify(e));
	 };

	 activity.onStop = function(e) {
	 Ti.API.info('MJR alloy onStop ' + JSON.stringify(e));
	 };
	 */
}